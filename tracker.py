#pip install --proxy proxy:8080 urllib2
import requests
#pip install --proxy proxy:8080 bs4
from bs4 import BeautifulSoup
# pip install --proxy proxy:8080 ansicolors
from colors import black, red, green, yellow, blue, magenta, cyan, white
from datetime import datetime
import sys

## Expression of interest case.
#
class CCase(object):
	## Username of case.
	#
	m_Username = ""
	## EOI date of effect.
	# 
	m_EoiDateOfEffect = datetime.today
	## EOI score.
	#
	m_Score = 0
	## EOI status.
	#
	m_Status = 'Unknown'
	## Constructor.
	#
	def __init__(self, p_Username, p_EoiDateOfEffect, p_Score, p_Status):
		self.m_Username = p_Username
		self.m_EoiDateOfEffect = p_EoiDateOfEffect
		self.m_Score = p_Score
		self.m_Status = p_Status
	## Representation.
	#
	def __repr__(self):
		return str(self.m_EoiDateOfEffect) + ' - ' + str(self.m_Score) + ' - ' + self.m_Status
	## String.
	#
	def __str__(self):
		return str(self.m_EoiDateOfEffect) + ' - ' + str(self.m_Score) + ' - ' + self.m_Status
		

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
	
class CELLS:
	COMMENTS = 0
	WATCH = 1
	DATE_CREATED = 2
	DATE_LAST_UPDATED = 3 
	USERNAME = 4 
	NATIONALITY = 5 
	ANZSCO_CODE = 6 
	OCCUPATION = 7 
	POINTS = 8 
	EOI_DATE_OF_EFFECT = 9
	DATE_INVITED = 10
	STATUS = 11 
	DAYS_TO_INVITE = 12 

http_proxy  = "http://proxy:8080"
https_proxy = "https://proxy:8080"
ftp_proxy   = "ftp://proxy:8080"

proxyDict = { 
			  "http"  : http_proxy, 
			  "https" : https_proxy, 
			  "ftp"   : ftp_proxy
			}

def scrapeURL(hostname, url_to_scrape, username, caseList):
	ownCase = None
	r = requests.get(hostname + url_to_scrape)
#	r = requests.get(hostname + url_to_scrape, proxies=proxyDict)
	soup = BeautifulSoup(r.text, 'html.parser')
	all_tables = soup.find_all('table')
	right_table = soup.find('table', id='original')
	rows = right_table.findAll("tr")
	rowCount = 0
	for row in rows:
		if(rowCount >= 2): # Skip first two rows with headings.
			cells = row.findAll('td')
			case = CCase(cells[CELLS.USERNAME].string, datetime.strptime(cells[CELLS.EOI_DATE_OF_EFFECT].string, '%d/%m/%Y'), int(cells[CELLS.POINTS].string), cells[CELLS.STATUS].string)
			caseList.append(case)
			if (case.m_Username == username):
				ownCase = case

			
		rowCount += 1
		
	# Check for for more data and initiate loading recursively
	nextlink = soup.find('a', rel='next', href=True)
	if nextlink is not None:
		oc = scrapeURL(hostname, nextlink['href'], username, caseList)
		if(ownCase == None):
			ownCase = oc
	
	return ownCase
	
## Main routine.
#
###############################################################################
def main():
	if(len(sys.argv) != 3):
		print('Tracker analyzer 1.0')
		print('Please specify username and and ANZSCO code')
		print('python tracker.py USERNAME ANZSCO')
		return

	username = sys.argv[1]
	anzsco = sys.argv[2]
	print('Scrapping data for user: ' + username + ' with ANZSCO code: ' + anzsco)
	
	hostname='https://myimmitracker.com'
	url_to_scrape_tmpl = '/en/trackers/expression-of-interest-sc189?filter%5B_active_slash_inactive_%5D=&filter%5B_anzsco_code_%5D=$$ANZSCO$$&filter%5B_days_to_invite_%5D=&filter%5B_eoi_date_of_effect_%5D=&filter%5B_invited_%5D=&filter%5B_nationality_%5D=&filter%5B_occupation_%5D=&filter%5B_points_%5D=&filter%5B_status_%5D=&filter%5B_username_%5D=&order_by%5B_eoi_date_of_effect_%5D=desc'
	url_to_scrape = url_to_scrape_tmpl.replace('$$ANZSCO$$', anzsco)
	
	cases = []
	ownCase = scrapeURL(hostname, url_to_scrape, username, cases)
	
	# Statistic evaluation
	casesInFront = 0
	lastClearedCase = None
	for case in cases:
		if (case.m_Status == 'Submitted' and case.m_Score >= ownCase.m_Score):
			if (case == ownCase):
				color = bcolors.OKBLUE
			elif (case.m_Status == "Invited"):
				color = bcolors.OKGREEN
			elif (case.m_Status == "Submitted"):
				color = bcolors.WARNING
			else:
				color = bcolors.ENDC
			print(color + str(case))
		
		if (case != ownCase and case.m_Status == 'Submitted'):
			if(case.m_Score > ownCase.m_Score):
				casesInFront += 1
			elif(case.m_Score == ownCase.m_Score and case.m_EoiDateOfEffect <= ownCase.m_EoiDateOfEffect):
				casesInFront += 1
			
		if(case.m_Score == ownCase.m_Score and case.m_Status == 'Invited' and lastClearedCase == None):
			lastClearedCase = case
			print(bcolors.OKGREEN + str(lastClearedCase))
			break

	print(bcolors.ENDC + 'Total cases found: ' + str(len(cases)))
	print(bcolors.OKBLUE  + '  Our own case is: ' + str(ownCase))
	print(bcolors.OKGREEN + 'Last cleared case: ' + str(lastClearedCase))
	print(bcolors.HEADER  + '   Cases in front: ' + str(casesInFront))
	print(bcolors.ENDC + 'Programm finished...')

## Startup.
#
###############################################################################
if __name__ == "__main__":
    main()