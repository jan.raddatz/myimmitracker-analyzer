myimmitracker-analyzer v 1.0
============================

This tool was developed in order to analyze the data on myimmitracker.com.
It helps you to estimate a possible invitation date after lodging your
expression of interest at skillselect.

Prerequesites
=============
Since myimmitracker-analyzer is written in python, you must have python 
installed. https://www.python.org/

In windows make sure that pythin is added to the environment variable during 
installation!

Setup
=====
Before running tracker.py you will have to install the following additional
python libraries because they do not come with python per default.

- BeautifulSoup
- requests 
- colors

To install these packages open a command shell and enter the following commands:
pip install requests
pip install bs4
pip install ansicolors

Adjusting the script (proxy settings)
=====================================
In case your PC is sitting behind a proxy you need to enter your proxy data
into the script in lines 67 - 69.
Then comment line 79 by putting # in front and uncomment line 80 by removing
the leading #.
Per default the script is assuming that you are not using a proxy server.

Running tracker.py
==================
To run the application open a command shell. Navigate to the folder where you 
put tracker.py. Start the application by typing the following command:
python tracker.py YOUUSERNAME YOURANZSCOCODE

For example:
At myimmitracker.com I have the username sphider.
My ANZSCO code is 261313. 

So I will call tracker.py like this:
python tracker.py sphider 2613

I ONLY ENTER THE FIRST 4 digits of the ANZSCO code in order to make sure to 
include all relevant subclasses.

The output will look something like this:

python tracker.py sphider 2613

Scrapping data for user: sphider with ANZSCO code: 2613

2016-08-03 00:00:00 - 65 - Submitted

2016-08-01 00:00:00 - 65 - Submitted

2016-08-01 00:00:00 - 65 - Submitted

2016-07-30 00:00:00 - 65 - Submitted

2016-07-29 00:00:00 - 65 - Submitted

2016-07-29 00:00:00 - 65 - Submitted

2016-07-27 00:00:00 - 65 - Submitted

2016-07-26 00:00:00 - 65 - Submitted

2016-07-26 00:00:00 - 65 - Submitted

2016-07-25 00:00:00 - 65 - Submitted

2016-07-21 00:00:00 - 65 - Submitted

2016-07-21 00:00:00 - 65 - Submitted

2016-07-20 00:00:00 - 65 - Submitted

2016-07-18 00:00:00 - 65 - Submitted

2016-07-15 00:00:00 - 65 - Submitted

2016-07-14 00:00:00 - 65 - Submitted

2016-07-13 00:00:00 - 65 - Submitted

2016-07-12 00:00:00 - 65 - Submitted

2016-07-10 00:00:00 - 65 - Submitted YOUR EOI

2016-07-08 00:00:00 - 65 - Submitted

2016-07-08 00:00:00 - 65 - Submitted

2016-07-07 00:00:00 - 65 - Submitted

2016-07-06 00:00:00 - 65 - Submitted

2016-07-05 00:00:00 - 65 - Submitted

2016-07-05 00:00:00 - 65 - Submitted

2016-07-05 00:00:00 - 65 - Submitted

2016-07-05 00:00:00 - 65 - Submitted

2016-07-04 00:00:00 - 65 - Submitted

2016-07-01 00:00:00 - 65 - Submitted

2016-07-01 00:00:00 - 65 - Submitted

2016-06-30 00:00:00 - 65 - Submitted

2016-06-29 00:00:00 - 65 - Submitted

2016-06-29 00:00:00 - 65 - Submitted

2016-06-29 00:00:00 - 65 - Submitted

2016-06-22 00:00:00 - 65 - Submitted

2016-06-21 00:00:00 - 65 - Submitted

2016-06-20 00:00:00 - 65 - Submitted

2016-06-19 00:00:00 - 65 - Submitted

2016-06-18 00:00:00 - 65 - Submitted

2016-06-18 00:00:00 - 65 - Submitted

2016-06-18 00:00:00 - 65 - Submitted

2016-06-17 00:00:00 - 65 - Submitted

2016-06-17 00:00:00 - 65 - Submitted

2016-06-16 00:00:00 - 65 - Submitted

2016-06-15 00:00:00 - 65 - Submitted

2016-06-15 00:00:00 - 65 - Submitted

2016-06-14 00:00:00 - 65 - Submitted

2016-06-14 00:00:00 - 65 - Submitted

2016-06-11 00:00:00 - 65 - Submitted

2016-06-09 00:00:00 - 65 - Submitted

2016-06-08 00:00:00 - 65 - Submitted

2016-06-07 00:00:00 - 65 - Submitted

2016-06-07 00:00:00 - 65 - Submitted

2016-06-06 00:00:00 - 65 - Submitted

2016-06-03 00:00:00 - 65 - Invited



Total cases found: 443

  Our own case is: 2016-07-10 00:00:00 - 65 - Submitted

Last cleared case: 2016-06-03 00:00:00 - 65 - Invited

   Cases in front: 35

Programm finished...


